# NAME

**list** - List all known CPrAN plugins

# SYNOPSIS

cpran list \[options\]

# DESCRIPTION

List plugins available through the CPrAN catalog.

**list** will show a list of all plugins available to CPrAN.

# EXAMPLES

    # Show all available plugins
    cpran list

# AUTHOR

José Joaquín Atria <jjatria@gmail.com>

# LICENSE

Copyright 2015 José Joaquín Atria

This program is free software; you may redistribute it and/or modify it under
the same terms as Perl itself.

# SEE ALSO

[CPrAN](cpran),
[CPrAN::Plugin](plugin),
[CPrAN::Command::install](install),
[CPrAN::Command::remove](remove),
[CPrAN::Command::search](search),
[CPrAN::Command::show](show),
[CPrAN::Command::test](test),
[CPrAN::Command::update](update),
[CPrAN::Command::upgrade](upgrade)
